#!/usr/bin/env bash

docker buildx build \
  -t kfkskillbyte/color-session:latest \
  -f Dockerfile \
  --build-arg COLOR=FF0000 \
  --platform linux/arm64,linux/amd64 \
  --push \
  .


build_color() {
  color=$1
  echo $color
  color_hex=$(echo $color | sed 's/,.*//')
  color_name=$(echo $color | sed 's/.*,//')
  docker buildx build \
    -t kfkskillbyte/color-session:$color_name \
    -f Dockerfile \
    --build-arg COLOR_OVERRIDE=$color_hex \
    --platform linux/arm64,linux/amd64 \
    --push \
    -q \
    .
}


N=20
(
for color in $(cat websafe-colors.csv)
do
   ((i=i%N)); ((i++==0)) && wait
   build_color $color &
done
wait
)
