#![allow(clippy::no_effect_underscore_binding)]

use std::env;

use lazy_static::lazy_static;
use prometheus::{register_int_counter, Encoder, IntCounter, TextEncoder};
use rand::distributions::Alphanumeric;
use rand::Rng;
use rocket::http::{Cookie, CookieJar};
use rocket::response::content::RawHtml;
use rocket::response::Redirect;
use rocket::{get, launch, routes, uri};

fn opposite_color(hex_color: &str) -> String {
    let color = i32::from_str_radix(hex_color, 16).unwrap_or_default();
    let r = (255 - (color >> 16)) & 0xFF;
    let g = (255 - (color >> 8)) & 0xFF;
    let b = (255 - color) & 0xFF;
    format!("{r:02X}{g:02X}{b:02X}")
}

fn detertime_color() -> String {
    match env::var("COLOR_OVERRIDE") {
        Ok(color_override) if !color_override.is_empty() => color_override,
        _ => {
            format!(
                "{:02X}{:02X}{:02X}",
                rand::random::<u8>(),
                rand::random::<u8>(),
                rand::random::<u8>()
            )
        }
    }
}

fn generate_random_string(length: usize) -> String {
    rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(length)
        .map(char::from)
        .collect()
}

#[get("/")]
fn index(cookies: &CookieJar<'_>) -> RawHtml<String> {
    CALL_COUNTER.inc();
    let random_color = cookies.get(COOKIE_NAME.as_str()).map_or_else(
        || {
            let random_color = detertime_color();
            cookies.add(Cookie::new(COOKIE_NAME.as_str(), random_color.clone()));
            random_color
        },
        |cookie| cookie.value().to_string(),
    );

    let opposite_color = opposite_color(&random_color);

    RawHtml(format!(
        "<!DOCTYPE html>
        <html>
        <head><title>Random Color</title></head>
        <body style='background-color: #{random_color}; color: #{opposite_color}; text-align: center; height: 100vh; display: flex; align-items: center; justify-content: center;'>
            <h1>{random_color}</h1>
        </body>
        </html>"
    ))
}

#[get("/clear")]
fn clear(cookies: &CookieJar<'_>) -> Redirect {
    CALL_COUNTER.inc();
    cookies.remove(COOKIE_NAME.as_str());
    Redirect::to(uri!(index))
}

#[get("/metrics")]
fn metrics() -> String {
    CALL_COUNTER.inc();
    let metric_families = prometheus::gather();
    let mut buffer = vec![];
    let encoder = TextEncoder::new();
    let _ = encoder.encode(&metric_families, &mut buffer);
    String::from_utf8(buffer).unwrap_or_default()
}

lazy_static! {
    static ref CALL_COUNTER: IntCounter =
        register_int_counter!("call_counter", "Count calls made to this application").unwrap();
    static ref COOKIE_NAME: String = generate_random_string(12);
}

#[launch]
fn rocket() -> _ {
    rocket::build().mount("/", routes![index, clear, metrics])
}
