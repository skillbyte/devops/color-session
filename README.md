# color-session

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

A simple webserver that displays random colors

By default the server displays a random color to each user, storing the random color in a cookie. Multiple instance will
display different colors to the same user as the cookie name is unique and random for each instance.

With the `/clear` endpoint a user can void their cookie and display a freshly chosen random color.

The `/metrics` endpoint displays OpenMetrics metrics including a call counter (but only for the entire site, not
differentiated by endpoint).

You can set a fixed color to be displayed always and to every user by supplying a hex code in the `COLOR_OVERRIDE`
environment variable. For added convenience there are 139 tags in addition to `latest` on the container image, each named after the web-safe
color it displays. See [Color Tags](#color-tags) for the complete list.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Color Tags](#color-tags)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

```sh
docker pull kfkskillbyte/color-session:latest
```

## Usage

```sh
docker run --rm -p 8000:8000 kfkskillbyte/color-session:latest
# OR manually override the color
docker run --rm -p 8000:8000 -e COLOR_OVERRIDE=21c1ee kfkskillbyte/color-session:latest
# OR use a color tag
docker run --rm -p 8000:8000 kfkskillbyte/color-session:deeppink
# OR use any of the 138 other color tags in the table below
```

You can also run from source with the following command (assuming you have the Rust toolchain installed):

```sh
cargo run
```

## Color Tags

The available color tags are:

| Column A-C     | Column C-D     | Column D-F    | Column F-L  | Column L             | Column L-M        | Column M-P    | Column P-S  | Column S-W  | Column W-Y  |
|----------------|----------------|---------------|-------------|----------------------|-------------------|---------------|-------------|-------------|-------------|
| aliceblue      | chocolate      | darkorchid    | fuchsia     | lavenderblush        | lightyellow       | midnightblue  | papayawhip  | sienna      | white       |
| antiquewhite   | coral          | darkred       | gainsboro   | lawngreen            | lime              | mintcream     | peachpuff   | silver      | whitesmoke  |
| aqua           | cornflowerblue | darksalmon    | ghostwhite  | lemonchiffon         | limegreen         | mistyrose     | peru        | skyblue     | yellow      |
| aquamarine     | cornsilk       | darkseagreen  | gold        | lightblue            | linen             | moccasin      | pink        | slateblue   | yellowgreen |
| azure          | crimson        | darkslateblue | goldenrod   | lightcoral           | magenta           | navajowhite   | plum        | slategray   |             |
| beige          | cyan           | darkslategray | gray        | lightcyan            | maroon            | navy          | powderblue  | snow        |             |
| bisque         | darkblue       | darkturquoise | green       | lightgoldenrodyellow | mediumaquamarine  | oldlace       | purple      | springgreen |             |
| black          | darkcyan       | darkviolet    | greenyellow | lightgreen           | mediumblue        | olive         | red         | steelblue   |             |
| blanchedalmond | darkgoldenrod  | deeppink      | honeydew    | lightgrey            | mediumorchid      | olivedrab     | rosybrown   | tan         |             |
| blue           | darkgray       | deepskyblue   | hotpink     | lightpink            | mediumpurple      | orange        | royalblue   | teal        |             |
| blueviolet     | darkgreen      | dimgray       | indianred   | lightsalmon          | mediumseagreen    | orangered     | saddlebrown | thistle     |             |
| brown          | darkkhaki      | dodgerblue    | indigo      | lightseagreen        | mediumslateblue   | orchid        | salmon      | tomato      |             |
| burlywood      | darkmagenta    | firebrick     | ivory       | lightskyblue         | mediumspringgreen | palegoldenrod | sandybrown  | turquoise   |             |
| cadetblue      | darkolivegreen | floralwhite   | khaki       | lightslategray       | mediumturquoise   | palegreen     | seagreen    | violet      |             |
| chartreuse     | darkorange     | forestgreen   | lavender    | lightsteelblue       | mediumvioletred   | palevioletred | seashell    | wheat       |             |

See section above for their usage.

## Maintainers

[@kfkonrad](https://gitlab.com/kfkonrad)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the
[standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2023 Kevin F. Konrad
