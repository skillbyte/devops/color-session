# syntax=docker/dockerfile:1


ARG RUST_VERSION=1.74.1
ARG APP_NAME=colorsession
FROM rust:${RUST_VERSION}-slim-bullseye AS build
ARG APP_NAME
WORKDIR /app

RUN --mount=type=bind,source=src,target=src \
    --mount=type=bind,source=Cargo.toml,target=Cargo.toml \
    --mount=type=bind,source=Cargo.lock,target=Cargo.lock \
    --mount=type=cache,target=/app/target/ \
    --mount=type=cache,target=/usr/local/cargo/registry/ \
    <<EOF
set -e
cargo build --locked --release
cp ./target/release/$APP_NAME /bin/server
EOF

FROM debian:bullseye-slim AS user-mgmt

# Create a non-privileged user that the app will run under.
# See https://docs.docker.com/go/dockerfile-user-best-practices/
ARG UID=10001
RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    appuser

FROM gcr.io/distroless/cc

COPY --from=user-mgmt /etc/passwd /etc/passwd
COPY --from=user-mgmt /etc/group /etc/group
USER appuser

COPY --from=build /bin/server /bin/

ENV ROCKET_ADDRESS=0.0.0.0

ARG COLOR_OVERRIDE
ENV COLOR_OVERRIDE=$COLOR_OVERRIDE

EXPOSE 8000

CMD ["/bin/server"]
